package com.example.carconfigurator.facades;

import com.example.carconfigurator.dtos.requests.ModeloRequestDTO;
import com.example.carconfigurator.dtos.responses.ModeloResponseDTO;

import java.util.List;

public interface ModeloFacade {

  ModeloResponseDTO update(Long id, ModeloRequestDTO modeloRequestDTO);
  ModeloResponseDTO save(ModeloRequestDTO modeloRequestDTO);
  ModeloResponseDTO findById(Long id);
  List<ModeloResponseDTO> findAll();
  void deleteById(Long id);

}