package com.example.carconfigurator.facades;

import com.example.carconfigurator.dtos.requests.MarcaRequestDTO;
import com.example.carconfigurator.dtos.responses.MarcaResponseDTO;

import java.util.List;

public interface MarcaFacade {

  MarcaResponseDTO update(Long id, MarcaRequestDTO marcaRequestDTO);
  MarcaResponseDTO save(MarcaRequestDTO marcaRequestDTO);
  MarcaResponseDTO findById(Long id);
  List<MarcaResponseDTO> findAll();
  void deleteById(Long id);

}