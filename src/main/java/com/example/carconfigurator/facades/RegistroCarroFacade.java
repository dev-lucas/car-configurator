package com.example.carconfigurator.facades;

import com.example.carconfigurator.dtos.requests.RegistroCarroRequestDTO;
import com.example.carconfigurator.dtos.responses.RegistroCarroResponseDTO;

import java.util.List;

public interface RegistroCarroFacade {

  RegistroCarroResponseDTO update(Long id, RegistroCarroRequestDTO registroCarroRequestDTO);
  RegistroCarroResponseDTO save(RegistroCarroRequestDTO registroCarroRequestDTO);
  RegistroCarroResponseDTO findById(Long id);
  List<RegistroCarroResponseDTO> findAll();
  void deleteById(Long id);

}