package com.example.carconfigurator.facades.impl;

import com.example.carconfigurator.dtos.requests.ModeloRequestDTO;
import com.example.carconfigurator.dtos.responses.ModeloResponseDTO;
import com.example.carconfigurator.entities.ModeloEntity;
import com.example.carconfigurator.facades.ModeloFacade;
import com.example.carconfigurator.services.ModeloService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ModeloFacadeImpl implements ModeloFacade {

  @Autowired
  private ModeloService modeloService;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public ModeloResponseDTO update(Long id, ModeloRequestDTO modeloRequestDTO) {
    return convertModeloEntityToModeloResponseDto(modeloService.update(id, convertModeloRequestDtoToModeloEntity(modeloRequestDTO)));
  }

  @Override
  public ModeloResponseDTO save(ModeloRequestDTO modeloRequestDTO) {
    return convertModeloEntityToModeloResponseDto(modeloService.save(convertModeloRequestDtoToModeloEntity(modeloRequestDTO)));
  }

  @Override
  public ModeloResponseDTO findById(Long id) {
    return convertModeloEntityToModeloResponseDto(modeloService.findById(id));
  }

  @Override
  public List<ModeloResponseDTO> findAll() {
    List<ModeloResponseDTO> modeloResponseDTOS = new ArrayList<>();
    for (ModeloEntity modeloEntity : modeloService.findAll()) {
      modeloResponseDTOS.add(convertModeloEntityToModeloResponseDto(modeloEntity));
    }
    return modeloResponseDTOS;
  }

  @Override
  public void deleteById(Long id) {
    modeloService.deleteById(id);
  }

  private ModeloEntity convertModeloRequestDtoToModeloEntity(ModeloRequestDTO modeloRequestDTO) {
    return modelMapper.map(modeloRequestDTO, ModeloEntity.class);
  }

  private ModeloResponseDTO convertModeloEntityToModeloResponseDto(ModeloEntity modeloEntity) {
    return modelMapper.map(modeloEntity, ModeloResponseDTO.class);
  }

}