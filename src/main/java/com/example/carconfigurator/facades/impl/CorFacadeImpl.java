package com.example.carconfigurator.facades.impl;

import com.example.carconfigurator.dtos.requests.CorRequestDTO;
import com.example.carconfigurator.dtos.responses.CorResponseDTO;
import com.example.carconfigurator.entities.CorEntity;
import com.example.carconfigurator.facades.CorFacade;
import com.example.carconfigurator.services.CorService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CorFacadeImpl implements CorFacade {

  @Autowired
  private CorService corService;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public CorResponseDTO update(Long id, CorRequestDTO corRequestDTO) {
    return convertCorEntityToCorResponseDto(corService.update(id, convertCorRequestDtoToCorEntity(corRequestDTO)));
  }

  @Override
  public CorResponseDTO save(CorRequestDTO corRequestDTO) {
    return convertCorEntityToCorResponseDto(corService.save(convertCorRequestDtoToCorEntity(corRequestDTO)));
  }

  @Override
  public CorResponseDTO findById(Long id) {
    return convertCorEntityToCorResponseDto(corService.findById(id));
  }

  @Override
  public List<CorResponseDTO> findAll() {
    List<CorResponseDTO> corResponseDTOS = new ArrayList<>();
    for (CorEntity corEntity : corService.findAll()) {
      corResponseDTOS.add(convertCorEntityToCorResponseDto(corEntity));
    }
    return corResponseDTOS;
  }

  @Override
  public void deleteById(Long id) {
    corService.deleteById(id);
  }

  private CorEntity convertCorRequestDtoToCorEntity(CorRequestDTO corRequestDTO) {
    return modelMapper.map(corRequestDTO, CorEntity.class);
  }

  private CorResponseDTO convertCorEntityToCorResponseDto(CorEntity corEntity) {
    return modelMapper.map(corEntity, CorResponseDTO.class);
  }

}