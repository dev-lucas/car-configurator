package com.example.carconfigurator.facades.impl;

import com.example.carconfigurator.dtos.requests.RegistroCarroRequestDTO;
import com.example.carconfigurator.dtos.responses.RegistroCarroResponseDTO;
import com.example.carconfigurator.entities.RegistroCarroEntity;
import com.example.carconfigurator.facades.RegistroCarroFacade;
import com.example.carconfigurator.services.RegistroCarroService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RegistroCarroFacadeImpl implements RegistroCarroFacade {

  @Autowired
  private RegistroCarroService registroCarroService;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public RegistroCarroResponseDTO update(Long id, RegistroCarroRequestDTO registroCarroRequestDTO) {
    return convertRegistroCarroEntityToRegistroCarroResponseDto(registroCarroService.update(id, convertRegistroCarroRequestDtoToRegistroCarroEntity(registroCarroRequestDTO)));
  }

  @Override
  public RegistroCarroResponseDTO save(RegistroCarroRequestDTO registroCarroRequestDTO) {
    return convertRegistroCarroEntityToRegistroCarroResponseDto(registroCarroService.save(convertRegistroCarroRequestDtoToRegistroCarroEntity(registroCarroRequestDTO)));
  }

  @Override
  public RegistroCarroResponseDTO findById(Long id) {
    return convertRegistroCarroEntityToRegistroCarroResponseDto(registroCarroService.findById(id));
  }

  @Override
  public List<RegistroCarroResponseDTO> findAll() {
    List<RegistroCarroResponseDTO> registroCarroResponseDTOS = new ArrayList<>();
    for (RegistroCarroEntity registroCarroEntity : registroCarroService.findAll()) {
      registroCarroResponseDTOS.add(convertRegistroCarroEntityToRegistroCarroResponseDto(registroCarroEntity));
    }
    return registroCarroResponseDTOS;
  }

  @Override
  public void deleteById(Long id) {
    registroCarroService.deleteById(id);
  }

  private RegistroCarroEntity convertRegistroCarroRequestDtoToRegistroCarroEntity(RegistroCarroRequestDTO registroCarroRequestDTO) {
    return modelMapper.map(registroCarroRequestDTO, RegistroCarroEntity.class);
  }

  private RegistroCarroResponseDTO convertRegistroCarroEntityToRegistroCarroResponseDto(RegistroCarroEntity registroCarroEntity) {
    return modelMapper.map(registroCarroEntity, RegistroCarroResponseDTO.class);
  }

}