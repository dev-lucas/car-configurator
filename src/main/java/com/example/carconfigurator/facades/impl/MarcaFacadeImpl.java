package com.example.carconfigurator.facades.impl;

import com.example.carconfigurator.dtos.requests.MarcaRequestDTO;
import com.example.carconfigurator.dtos.responses.MarcaResponseDTO;
import com.example.carconfigurator.entities.MarcaEntity;
import com.example.carconfigurator.facades.MarcaFacade;
import com.example.carconfigurator.services.MarcaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MarcaFacadeImpl implements MarcaFacade {

  @Autowired
  private MarcaService marcaService;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public MarcaResponseDTO update(Long id, MarcaRequestDTO marcaRequestDTO) {
    return convertMarcaEntityToMarcaResponseDto(marcaService.update(id,convertMarcaRequestDtoToMarcaEntity(marcaRequestDTO)));
  }

  @Override
  public MarcaResponseDTO save(MarcaRequestDTO marcaRequestDTO) {
    return convertMarcaEntityToMarcaResponseDto(marcaService.save(convertMarcaRequestDtoToMarcaEntity(marcaRequestDTO)));
  }

  @Override
  public MarcaResponseDTO findById(Long id) {
    return convertMarcaEntityToMarcaResponseDto(marcaService.findById(id));
  }

  @Override
  public List<MarcaResponseDTO> findAll() {
    List<MarcaResponseDTO> marcaResponseDTOS = new ArrayList<>();
    for (MarcaEntity marcaEntity : marcaService.findAll()) {
      marcaResponseDTOS.add(convertMarcaEntityToMarcaResponseDto(marcaEntity));
    }
    return marcaResponseDTOS;
  }

  @Override
  public void deleteById(Long id) {
    marcaService.deleteById(id);
  }

  private MarcaEntity convertMarcaRequestDtoToMarcaEntity(MarcaRequestDTO marcaRequestDTO) {
    return modelMapper.map(marcaRequestDTO, MarcaEntity.class);
  }

  private MarcaResponseDTO convertMarcaEntityToMarcaResponseDto(MarcaEntity marcaEntity) {
    return modelMapper.map(marcaEntity, MarcaResponseDTO.class);
  }

}