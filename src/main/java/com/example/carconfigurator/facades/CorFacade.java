package com.example.carconfigurator.facades;

import com.example.carconfigurator.dtos.requests.CorRequestDTO;
import com.example.carconfigurator.dtos.responses.CorResponseDTO;

import java.util.List;

public interface CorFacade {

  CorResponseDTO update(Long id, CorRequestDTO corRequestDTO);
  CorResponseDTO save(CorRequestDTO corRequestDTO);
  CorResponseDTO findById(Long id);
  List<CorResponseDTO> findAll();
  void deleteById(Long id);

}