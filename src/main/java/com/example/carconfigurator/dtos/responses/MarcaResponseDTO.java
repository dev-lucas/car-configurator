package com.example.carconfigurator.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarcaResponseDTO {

  private Long id;
  private String nomeDaMarca;

}