package com.example.carconfigurator.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistroCarroRequestDTO {

 private Long id;
 private Long idDaMarca;
 private Long idDaCor;
 private Long idDoModelo;
 private String nomeDoDono;

}