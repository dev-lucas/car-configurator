package com.example.carconfigurator.dtos.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarcaRequestDTO {

  private Long id;
  private String nomeDaMarca;

}