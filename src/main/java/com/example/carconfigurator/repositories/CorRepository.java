package com.example.carconfigurator.repositories;

import com.example.carconfigurator.entities.CorEntity;
import org.springframework.data.repository.CrudRepository;

public interface CorRepository extends CrudRepository<CorEntity, Long> { }