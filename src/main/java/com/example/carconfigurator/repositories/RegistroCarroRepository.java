package com.example.carconfigurator.repositories;

import com.example.carconfigurator.entities.RegistroCarroEntity;
import org.springframework.data.repository.CrudRepository;

public interface RegistroCarroRepository extends CrudRepository<RegistroCarroEntity, Long> { }