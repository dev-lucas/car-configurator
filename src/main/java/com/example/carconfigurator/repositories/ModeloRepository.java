package com.example.carconfigurator.repositories;

import com.example.carconfigurator.entities.ModeloEntity;
import org.springframework.data.repository.CrudRepository;

public interface ModeloRepository extends CrudRepository<ModeloEntity, Long> { }