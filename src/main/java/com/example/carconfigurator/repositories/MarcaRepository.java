package com.example.carconfigurator.repositories;

import com.example.carconfigurator.entities.MarcaEntity;
import org.springframework.data.repository.CrudRepository;

public interface MarcaRepository extends CrudRepository<MarcaEntity, Long> { }