package com.example.carconfigurator.controllers;

import com.example.carconfigurator.dtos.requests.MarcaRequestDTO;
import com.example.carconfigurator.dtos.responses.MarcaResponseDTO;
import com.example.carconfigurator.facades.MarcaFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/marcas")
public class MarcaController {

  @Autowired
  private MarcaFacade marcaFacade;

  @PostMapping
  public MarcaResponseDTO save(@RequestBody MarcaRequestDTO marcaRequestDTO) {
    return marcaFacade.save(marcaRequestDTO);
  }

  @PutMapping("/{id}")
  public MarcaResponseDTO update(@PathVariable Long id, @RequestBody MarcaRequestDTO marcaRequestDTO) {
    return marcaFacade.update(id, marcaRequestDTO);
  }

  @GetMapping("/{id}")
  public MarcaResponseDTO findById(@PathVariable Long id) {
    return marcaFacade.findById(id);
  }

  @GetMapping
  public List<MarcaResponseDTO> findAll() {
    return marcaFacade.findAll();
  }

  @DeleteMapping("/{id}")
  public void deleteById(@PathVariable Long id) {
    marcaFacade.deleteById(id);
  }

}