package com.example.carconfigurator.controllers;

import com.example.carconfigurator.dtos.requests.CorRequestDTO;
import com.example.carconfigurator.dtos.responses.CorResponseDTO;
import com.example.carconfigurator.facades.CorFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cores")
public class CorController {

  @Autowired
  private CorFacade corFacade;

  @PostMapping
  public CorResponseDTO save(@RequestBody CorRequestDTO corRequestDTO) {
    return corFacade.save(corRequestDTO);
  }

  @PutMapping("/{id}")
  public CorResponseDTO update(@PathVariable Long id, @RequestBody CorRequestDTO corRequestDTO) {
    return corFacade.update(id, corRequestDTO);
  }

  @GetMapping("/{id}")
  public CorResponseDTO findById(@PathVariable Long id) {
    return corFacade.findById(id);
  }

  @GetMapping
  public List<CorResponseDTO> findAll() {
    return corFacade.findAll();
  }

  @DeleteMapping("/{id}")
  public void deleteById(@PathVariable Long id){
    corFacade.deleteById(id);
  }

}