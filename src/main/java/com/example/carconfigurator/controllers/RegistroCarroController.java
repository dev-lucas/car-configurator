package com.example.carconfigurator.controllers;

import com.example.carconfigurator.dtos.requests.RegistroCarroRequestDTO;
import com.example.carconfigurator.dtos.responses.RegistroCarroResponseDTO;
import com.example.carconfigurator.facades.RegistroCarroFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/registros-carros")
public class RegistroCarroController {

  @Autowired
  private RegistroCarroFacade registroCarroFacade;

  @PostMapping
  public RegistroCarroResponseDTO save(@RequestBody RegistroCarroRequestDTO registroCarroRequestDTO) {
    return registroCarroFacade.save(registroCarroRequestDTO);
  }

  @PutMapping("/{id}")
  public RegistroCarroResponseDTO update(@PathVariable Long id, @RequestBody RegistroCarroRequestDTO registroCarroRequestDTO) {
    return registroCarroFacade.update(id, registroCarroRequestDTO);
  }

  @GetMapping("/{id}")
  public RegistroCarroResponseDTO findById(@PathVariable Long id) {
    return registroCarroFacade.findById(id);
  }

  @GetMapping
  public List<RegistroCarroResponseDTO> findAll() {
    return registroCarroFacade.findAll();
  }

  @DeleteMapping("/{id}")
  public void deleteById(@PathVariable Long id) {
    registroCarroFacade.deleteById(id);
  }

}