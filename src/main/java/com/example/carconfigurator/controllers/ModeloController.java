package com.example.carconfigurator.controllers;

import com.example.carconfigurator.dtos.requests.ModeloRequestDTO;
import com.example.carconfigurator.dtos.responses.ModeloResponseDTO;
import com.example.carconfigurator.facades.ModeloFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/modelos")
public class ModeloController {

  @Autowired
  private ModeloFacade modeloFacade;

  @PostMapping
  public ModeloResponseDTO save(@RequestBody ModeloRequestDTO modeloRequestDTO) {
    return modeloFacade.save(modeloRequestDTO);
  }

  @PutMapping("/{id}")
  public ModeloResponseDTO update(@PathVariable Long id, @RequestBody ModeloRequestDTO modeloRequestDTO) {
    return modeloFacade.update(id, modeloRequestDTO);
  }

  @GetMapping("/{id}")
  public ModeloResponseDTO findById(@PathVariable Long id) {
    return modeloFacade.findById(id);
  }

  @GetMapping
  public List<ModeloResponseDTO> findAll() {
    return modeloFacade.findAll();
  }

  @DeleteMapping("/{id}")
  public void deleteById(@PathVariable Long id) {
    modeloFacade.deleteById(id);
  }

}