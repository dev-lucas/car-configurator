package com.example.carconfigurator.services;

import com.example.carconfigurator.entities.CorEntity;
import com.example.carconfigurator.entities.MarcaEntity;

import java.util.List;

public interface CorService {

  CorEntity update(Long id, CorEntity corEntity);
  CorEntity save(CorEntity corEntity);
  CorEntity findById(Long id);
  List<CorEntity> findAll();
  void deleteById(Long id);

}