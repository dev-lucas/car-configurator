package com.example.carconfigurator.services;

import com.example.carconfigurator.entities.MarcaEntity;

import java.util.List;

public interface MarcaService {

  MarcaEntity update(Long id, MarcaEntity marcaEntity);
  MarcaEntity save(MarcaEntity marcaEntity);
  MarcaEntity findById(Long id);
  List<MarcaEntity> findAll();
  void deleteById(Long id);

}