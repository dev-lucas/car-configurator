package com.example.carconfigurator.services.impl;

import com.example.carconfigurator.entities.MarcaEntity;
import com.example.carconfigurator.exceptions.MarcaException;
import com.example.carconfigurator.exceptions.enums.MarcaEnum;
import com.example.carconfigurator.repositories.MarcaRepository;
import com.example.carconfigurator.services.MarcaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class MarcaServiceImpl implements MarcaService {

  @Autowired
  private MarcaRepository marcaRepository;

  @Override
  public MarcaEntity update(Long id, MarcaEntity marcaEntity) {
    checkIfMarcaExist(id);
    marcaEntity.setId(id);
    log.info("Alterando marca: {}, id:{}", marcaEntity,id);
    return marcaRepository.save(marcaEntity);
  }

  @Override
  public MarcaEntity save(MarcaEntity marcaEntity) {
    log.info("Salvando marca: {}", marcaEntity);
    return marcaRepository.save(marcaEntity);
  }

  @Override
  public MarcaEntity findById(Long id) {
    checkIfMarcaExist(id);
    log.info("Buscando marca por id: {}", id);
    return marcaRepository.findById(id).orElse(new MarcaEntity());
  }

  @Override
  public List<MarcaEntity> findAll() {
    log.info("Listando todas as marcas cadastradas");
    List<MarcaEntity> marcaEntities = new ArrayList<>();
    for (MarcaEntity marcaEntity : marcaRepository.findAll()) {
      marcaEntities.add(marcaEntity);
    }
    return marcaEntities;
  }

  @Override
  public void deleteById(Long id) {
    log.info("Deletando marca por id: {}", id);
    checkIfMarcaExist(id);
    marcaRepository.deleteById(id);
  }

  private void checkIfMarcaExist(Long id) {
    if (!marcaRepository.existsById(id)) {
      throw new MarcaException(MarcaEnum.MARCA_NAO_ENCONTRADA);
    }
  }
}