package com.example.carconfigurator.services.impl;

import com.example.carconfigurator.entities.CorEntity;
import com.example.carconfigurator.exceptions.CorException;
import com.example.carconfigurator.exceptions.enums.CorEnum;
import com.example.carconfigurator.repositories.CorRepository;
import com.example.carconfigurator.services.CorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CorServiceImpl implements CorService {

  @Autowired
  private CorRepository corRepository;


  @Override
  public CorEntity update(Long id, CorEntity corEntity) {
    checkIfCorExist(id);
    corEntity.setId(id);
    log.info("Alterando cor: {}, id:{}", corEntity,id);
    return corRepository.save(corEntity);
  }

  @Override
  public CorEntity save(CorEntity corEntity) {
    log.info("Salvando cor: {}", corEntity);
    return corRepository.save(corEntity);
  }

  @Override
  public CorEntity findById(Long id) {
    checkIfCorExist(id);
    log.info("Buscando cor por id: {}", id);
    return corRepository.findById(id).orElse(new CorEntity());
  }

  @Override
  public List<CorEntity> findAll() {
    log.info("Listando todas as cores cadastradas");
    List<CorEntity> corEntities = new ArrayList<>();
    for (CorEntity corEntity : corRepository.findAll()) {
      corEntities.add(corEntity);
    }
    return corEntities;
  }

  @Override
  public void deleteById(Long id) {
    log.info("Deletando cor por id: {}", id);
    checkIfCorExist(id);
    corRepository.deleteById(id);
  }

  private void checkIfCorExist(Long id) {
    if (!corRepository.existsById(id)) {
      throw new CorException(CorEnum.COR_NAO_ENCONTRADA);
    }
  }
}