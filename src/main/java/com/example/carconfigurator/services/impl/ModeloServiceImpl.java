package com.example.carconfigurator.services.impl;

import com.example.carconfigurator.entities.ModeloEntity;
import com.example.carconfigurator.exceptions.ModeloException;
import com.example.carconfigurator.exceptions.enums.ModeloEnum;
import com.example.carconfigurator.repositories.ModeloRepository;
import com.example.carconfigurator.services.ModeloService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ModeloServiceImpl implements ModeloService {

  @Autowired
  private ModeloRepository modeloRepository;

  @Override
  public ModeloEntity update(Long id, ModeloEntity modeloEntity) {
    checkIfModeloExist(id);
    modeloEntity.setId(id);
    log.info("Alterando modelo: {}, id:{}", modeloEntity,id);
    return modeloRepository.save(modeloEntity);
  }

  @Override
  public ModeloEntity save(ModeloEntity modeloEntity) {
    log.info("Salvando modelo: {}", modeloEntity);
    return modeloRepository.save(modeloEntity);
  }

  @Override
  public ModeloEntity findById(Long id) {
    checkIfModeloExist(id);
    log.info("Buscando modelo por id: {}", id);
    return modeloRepository.findById(id).orElse(new ModeloEntity());
  }

  @Override
  public List<ModeloEntity> findAll() {
    log.info("Listando todas os modelos cadastrados");
    List<ModeloEntity> modeloEntities = new ArrayList<>();
    for (ModeloEntity modeloEntity : modeloRepository.findAll()) {
      modeloEntities.add(modeloEntity);
    }
    return modeloEntities;
  }

  @Override
  public void deleteById(Long id) {
    log.info("Deletando modelo por id: {}", id);
    checkIfModeloExist(id);
    modeloRepository.deleteById(id);
  }

  private void checkIfModeloExist(Long id) {
    if (!modeloRepository.existsById(id)) {
      throw new ModeloException(ModeloEnum.MODELO_NAO_ENCONTRADO);
    }
  }
}