package com.example.carconfigurator.services.impl;

import com.example.carconfigurator.entities.RegistroCarroEntity;
import com.example.carconfigurator.exceptions.CorException;
import com.example.carconfigurator.exceptions.MarcaException;
import com.example.carconfigurator.exceptions.ModeloException;
import com.example.carconfigurator.exceptions.RegistroCarroException;
import com.example.carconfigurator.exceptions.enums.CorEnum;
import com.example.carconfigurator.exceptions.enums.MarcaEnum;
import com.example.carconfigurator.exceptions.enums.ModeloEnum;
import com.example.carconfigurator.exceptions.enums.RegistroCarroEnum;
import com.example.carconfigurator.repositories.CorRepository;
import com.example.carconfigurator.repositories.MarcaRepository;
import com.example.carconfigurator.repositories.ModeloRepository;
import com.example.carconfigurator.repositories.RegistroCarroRepository;
import com.example.carconfigurator.services.RegistroCarroService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class RegistroCarroServiceImpl implements RegistroCarroService {

  @Autowired
  private RegistroCarroRepository registroCarroRepository;

  @Autowired
  private MarcaRepository marcaRepository;

  @Autowired
  private CorRepository corRepository;

  @Autowired
  private ModeloRepository modeloRepository;

  @Override
  public RegistroCarroEntity update(Long id, RegistroCarroEntity registroCarroEntity) {
    log.info("Alterando registro do carro: {}, id:{}", registroCarroEntity, id);
    checkIfRegistroCarroExist(id);
    checkIfMarcaExist(registroCarroEntity.getIdDaMarca().getId());
    checkIfCorExist(registroCarroEntity.getIdDaCor().getId());
    checkIfModeloExist(registroCarroEntity.getIdDoModelo().getId());
    registroCarroEntity.setId(id);
    return registroCarroRepository.save(registroCarroEntity);
  }

  @Override
  public RegistroCarroEntity save(RegistroCarroEntity registroCarroEntity) {
    checkIfMarcaExist(registroCarroEntity.getIdDaMarca().getId());
    checkIfCorExist(registroCarroEntity.getIdDaCor().getId());
    checkIfModeloExist(registroCarroEntity.getIdDoModelo().getId());
    log.info("Salvando registro do carro: {}", registroCarroEntity);
    return registroCarroRepository.save(registroCarroEntity);
  }

  @Override
  public RegistroCarroEntity findById(Long id) {
    checkIfRegistroCarroExist(id);
    log.info("Buscando registro do carro por id: {}", id);
    return registroCarroRepository.findById(id).orElse(new RegistroCarroEntity());
  }

  @Override
  public List<RegistroCarroEntity> findAll() {
    log.info("Listando todos os registros dos carros cadastrados");
    List<RegistroCarroEntity> registroCarroEntities = new ArrayList<>();
    for (RegistroCarroEntity registroCarroEntity : registroCarroRepository.findAll()) {
      registroCarroEntities.add(registroCarroEntity);
    }
    return registroCarroEntities;
  }

  @Override
  public void deleteById(Long id) {
    checkIfRegistroCarroExist(id);
    registroCarroRepository.deleteById(id);
  }

  private void checkIfRegistroCarroExist(Long id) {
    if (!registroCarroRepository.existsById(id)) {
      throw new RegistroCarroException(RegistroCarroEnum.REGISTRO_CARRO_NAO_ENCONTRADO);
    }
  }

  private void checkIfMarcaExist(Long id) {
    if (!marcaRepository.existsById(id)) {
      throw new MarcaException(MarcaEnum.MARCA_NAO_ENCONTRADA);
    }
  }

  private void checkIfCorExist(Long id) {
    if (!corRepository.existsById(id)) {
      throw new CorException(CorEnum.COR_NAO_ENCONTRADA);
    }
  }

  private void checkIfModeloExist(Long id) {
    if (!modeloRepository.existsById(id)) {
      throw new ModeloException(ModeloEnum.MODELO_NAO_ENCONTRADO);
    }
  }

}