package com.example.carconfigurator.services;

import com.example.carconfigurator.entities.MarcaEntity;
import com.example.carconfigurator.entities.ModeloEntity;

import java.util.List;

public interface ModeloService {

  ModeloEntity update(Long id, ModeloEntity modeloEntity);
  ModeloEntity save(ModeloEntity modeloEntity);
  ModeloEntity findById(Long id);
  List<ModeloEntity> findAll();
  void deleteById(Long id);

}