package com.example.carconfigurator.services;

import com.example.carconfigurator.entities.RegistroCarroEntity;

import java.util.List;

public interface RegistroCarroService {

  RegistroCarroEntity update(Long id, RegistroCarroEntity registroCarroEntity);
  RegistroCarroEntity save(RegistroCarroEntity registroCarroEntity);
  RegistroCarroEntity findById(Long id);
  List<RegistroCarroEntity> findAll();
  void deleteById(Long id);

}