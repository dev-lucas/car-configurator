package com.example.carconfigurator.exceptions;

import com.example.carconfigurator.exceptions.enums.MarcaEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class MarcaException extends RuntimeException {
  private static final long serialVersionUID = -4589179341768493322L;
  private final MarcaEnum error;

  public MarcaException(MarcaEnum error) {
    super(error.getMessage());
    this.error = error;
  }
}