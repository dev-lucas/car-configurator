package com.example.carconfigurator.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum CorEnum {

  COR_NAO_ENCONTRADA("ABC-COR-001", "Cor nao encontrada", 400);

  private String code;
  private String message;
  private Integer statusCode;

  }