package com.example.carconfigurator.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum MarcaEnum {

  MARCA_NAO_ENCONTRADA("ABC-MARCA-001", "Marca nao encontrada", 400);

  private String code;
  private String message;
  private Integer statusCode;

}