package com.example.carconfigurator.exceptions.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ModeloEnum {

  MODELO_NAO_ENCONTRADO("ABC-MODELO-001", "Modelo nao encontrada", 400);

  private String code;
  private String message;
  private Integer statusCode;

}