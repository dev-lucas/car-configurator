package com.example.carconfigurator.exceptions;

import com.example.carconfigurator.exceptions.enums.ModeloEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class ModeloException extends RuntimeException {
  private static final long serialVersionUID = -4589179341768493322L;
  private final ModeloEnum error;

  public ModeloException(ModeloEnum error) {
    super(error.getMessage());
    this.error = error;
  }
}