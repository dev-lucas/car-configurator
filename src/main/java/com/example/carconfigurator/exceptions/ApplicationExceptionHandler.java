package com.example.carconfigurator.exceptions;

import com.example.carconfigurator.exceptions.models.ErrorObject;
import com.example.carconfigurator.exceptions.models.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

  private final String ERROR_MESSAGE = "Requisicao possui campos invalidos ou nao informados!";

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
    List<ErrorObject> errors = getErros(ex);
    ErrorResponse errorResponse = getErrorResponse(ex, status, errors);
    log.error("{error de entrada de dados!}", errorResponse);
    return new ResponseEntity<>(errorResponse, status);
  }

  private List<ErrorObject> getErros(MethodArgumentNotValidException ex) {
    return ex.getBindingResult().getFieldErrors().stream().map(
        error -> new ErrorObject(
            error.getDefaultMessage(),
            error.getField(),
            error.getRejectedValue()
        )).collect(Collectors.toList());
  }

  private ErrorResponse getErrorResponse(MethodArgumentNotValidException ex, HttpStatus status, List<ErrorObject> errors) {
    return new ErrorResponse(
        Instant.now().toEpochMilli(),
        status.getReasonPhrase(),
        status.value(),
        ERROR_MESSAGE,
        errors);
  }

  //Chave estrangeira
  @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
  public ResponseEntity<ErrorResponse> sqlIntegrityConstrainViolationException(Exception e) {
    log.error("CHAVE STRANGEIRA INCORRETA!");
    return new ResponseEntity<>(new ErrorResponse(
        Instant.now().toEpochMilli(),
        HttpStatus.BAD_REQUEST.getReasonPhrase(),
        HttpStatus.BAD_REQUEST.value(),
        "CHAVE ESTRANGEIRA INCORRETA!",
        null), HttpStatus.BAD_REQUEST);
  }

  //Id nao encontrado
  @ExceptionHandler(EmptyResultDataAccessException.class)
  public ResponseEntity<ErrorResponse> emptyResultDataAccessException(Exception e) {
    log.error("ID NAO ENCONTRADO!");
    return new ResponseEntity<>(new ErrorResponse(Instant.now().toEpochMilli(),
        HttpStatus.BAD_REQUEST.getReasonPhrase(),
        HttpStatus.BAD_REQUEST.value(),
        "ID NAO ENCONTRADO!",
        null), HttpStatus.BAD_REQUEST);
  }

  //Marca
  @ResponseBody
  @ExceptionHandler(MarcaException.class)
  ResponseEntity<ErrorResponse> handlerMarcaException(MarcaException e) {
    return ResponseEntity.status(e.getError().getStatusCode())
        .body((new ErrorResponse(Instant.now().toEpochMilli(),
            e.getError().getCode(),
            e.getError().getStatusCode(),
            e.getMessage(), new ArrayList<>())));
  }

  //Cor
  @ResponseBody
  @ExceptionHandler(CorException.class)
  ResponseEntity<ErrorResponse> handlerCorException(CorException e) {
    return ResponseEntity.status(e.getError().getStatusCode())
        .body((new ErrorResponse(Instant.now().toEpochMilli(),
            e.getError().getCode(),
            e.getError().getStatusCode(),
            e.getMessage(), new ArrayList<>())));
  }

  //Cor
  @ResponseBody
  @ExceptionHandler(ModeloException.class)
  ResponseEntity<ErrorResponse> handlerModeloException(ModeloException e) {
    return ResponseEntity.status(e.getError().getStatusCode())
        .body((new ErrorResponse(Instant.now().toEpochMilli(),
            e.getError().getCode(),
            e.getError().getStatusCode(),
            e.getMessage(), new ArrayList<>())));
  }

  //RegistroCarro
  @ResponseBody
  @ExceptionHandler(RegistroCarroException.class)
  ResponseEntity<ErrorResponse> handlerRegistroCarroException(RegistroCarroException e) {
    return ResponseEntity.status(e.getError().getStatusCode())
        .body((new ErrorResponse(Instant.now().toEpochMilli(),
            e.getError().getCode(),
            e.getError().getStatusCode(),
            e.getMessage(), new ArrayList<>())));
  }

}