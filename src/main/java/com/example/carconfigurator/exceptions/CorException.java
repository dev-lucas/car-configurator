package com.example.carconfigurator.exceptions;

import com.example.carconfigurator.exceptions.enums.CorEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class CorException extends RuntimeException {
  private static final long serialVersionUID = -4589179341768493322L;
  private final CorEnum error;

  public CorException(CorEnum error) {
    super(error.getMessage());
    this.error = error;
  }
}
