package com.example.carconfigurator.exceptions;

public class CarConfiguratorException extends RuntimeException {

  private static final long serialVersionsUID = 1L;

  public CarConfiguratorException(String message) {
    super(message);
  }
}
