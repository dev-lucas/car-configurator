package com.example.carconfigurator.exceptions;

import com.example.carconfigurator.exceptions.enums.RegistroCarroEnum;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class RegistroCarroException extends RuntimeException {
  private static final long serialVersionUID = -4589179341768493322L;
  private final RegistroCarroEnum error;

  public RegistroCarroException(RegistroCarroEnum error) {
    super(error.getMessage());
    this.error = error;
  }
}