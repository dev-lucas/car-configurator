package com.example.carconfigurator.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class RegistroCarroEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @ManyToOne
  private MarcaEntity idDaMarca;

  @ManyToOne
  private CorEntity idDaCor;

  @ManyToOne
  private ModeloEntity idDoModelo;

  private String nomeDoDono;

}